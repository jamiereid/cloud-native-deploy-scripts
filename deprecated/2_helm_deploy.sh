#!/bin/bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --debug --install gitlab gitlab/gitlab \
  --timeout 600 \
  --set global.hosts.domain=jreid.dev \
  --set certmanager-issuer.email=james.r.reid@gmail.com
