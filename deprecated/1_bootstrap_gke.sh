#!/bin/bash

# Prompt user for GCP project name
echo "Provide a GCP Project Name:"
read PROJECT

# Create new GCP Project
gcloud projects create $PROJECT

# Enable billing for the project
gcloud beta billing projects link $PROJECT --billing-account 01EAEB-F33690-3A30DE

# Set $PROJECT as default for gloud
gcloud config set core/project $PROJECT

# Enable required APIs
echo "Enabling gcloud services..."
gcloud services enable \
     cloudapis.googleapis.com \
     container.googleapis.com \
     containerregistry.googleapis.com
echo "Running GL bootstrap script..."
# export environment variables for gitlab gke bootstrap script
export REGION="us-west1"
export ZONE="us-west1-a"
export CLUSTER_NAME="gitlab-cluster"
export MACHINE_TYPE="n1-standard-4"
export NUM_NODES="2"
./scripts/gke_bootstrap_script.sh up
