Deploying Cloud Native Gitlab to Google Kubernetes Engine (GKE)
---
Following the directions available at https://docs.gitlab.com/charts/, the enclosed scripts are intended to aid the user in deploying a complete Gitlab instance within GKE. 

First, 1_bootstrap_gke.sh sets a handful of environment variables, and then triggers the GL GKE Bootstrap script, which will initialize a k8s cluster within the specified GCP project. Note it's important that billing is enabled, k8s APIs enabled, and a Google Cloud DNS domain present for external-dns to function as expected. If all goes well, Tiller is enabled/configured on the remote GKE cluster and step two can be followed:

Second, 2_helm_deploy.sh adds the GL chart repo, and deploys gitlab to the created GKE cluster.

Finally, 3_get_root_pw.sh retrieves the base64-encoded secret from the k8s cluster to identify the initial root user password.