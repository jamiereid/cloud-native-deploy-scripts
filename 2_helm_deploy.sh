#!/bin/bash
# 8/8/2019
# Jamie Reid

# run script in same folder where 'gitlab' folder exists from this repo:
# https://gitlab.com/jamiereid/gitlab

#update dependencies
helm dependency update ./gitlab

# deploy to cluster from local gitlab chart
helm upgrade --debug \
  --install -f gitlab/values.yaml -f gitlab/values-gke-minimum.yaml gitlab ./gitlab \
  --timeout 600 \
  --set global.hosts.domain=jreid.dev \
  --set certmanager-issuer.email=james.r.reid@gmail.com
