#!/bin/bash
# 8/8/2019 Jamie Reid

# Assumptions / Prerequisites
# ---
# GCP Project has been created
#
# Billing has been enabled for the project
# e.g. gcloud beta billing projects link $PROJECT --billing-account 0X0X0X-0X0X0X-0X0X0X
#
# Set $PROJECT as default for gcloud
# gcloud config set core/project $PROJECT
# 
# K8s APIs etc are enabled
# echo "Enabling gcloud services..."
# gcloud services enable \
#     cloudapis.googleapis.com \
#     container.googleapis.com \
#     containerregistry.googleapis.com

# export environment variables for gitlab gke bootstrap script
export PROJECT="jreid-profile"
export REGION="us-west2"
export ZONE="us-west2-b"
export CLUSTER_NAME="gitlab-cluster"
#export MACHINE_TYPE="n1-standard-4"
export MACHINE_TYPE="n1-standard-1"
export NUM_NODES="3"
echo "Running GL bootstrap script..."
./scripts/gke_bootstrap_script.sh up
